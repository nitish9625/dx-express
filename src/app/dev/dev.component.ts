import { Component, OnInit } from '@angular/core';
import { DxButtonModule, DxTemplateModule ,} from 'devextreme-angular';
import notify from "devextreme/ui/notify"

@Component({
  selector: 'app-dev',
  templateUrl: './dev.component.html',
  styleUrls: ['./dev.component.css']
})
export class DevComponent implements OnInit {
  HelloWorld(){
    alert("welcome to dev express");
  }

  ListItems = [{
    text:'cars',
    badge: 12
  },
  {
    text:'bike',
    badge: 5
  }
]

okClicked(){
  notify("The ok button was clicked")
}
bindingProperty: string = "Some value";
ListData = [
  {itemProperties:"some another value"},
  {itemName:"this is Second properties"}
]

  constructor() { 
  }
  

  ngOnInit(): void {
  }

}
