import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DevComponent } from './dev/dev.component';
import { SecondDevComponent } from './second-dev/second-dev.component';
import { ThirdDevComponent } from './third-dev/third-dev.component';

const routes: Routes = [
  // {path:"second-dev", component:SecondDevComponent},
  // {path:"third-dev", component:ThirdDevComponent}
  



  // {
  //   path:'dev-component', component:DevComponent, // parent link 
  //   children: [
  //     {path:'second-dev', component:SecondDevComponent},  // first child link
  //     {path:'third-dev', component:ThirdDevComponent} //second child link
  //   ]
  // }
  {path:'', component:DevComponent, data: {title:'this is home page'}},
  {path:'second', component:SecondDevComponent, data:{title:"this is second page"},
  children:[
    {path:'third', component:ThirdDevComponent, data:{title:'thhis is seond page'}}
  ]
}
];



@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
