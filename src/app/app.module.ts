import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DevComponent } from './dev/dev.component';

import { DxBulletModule, 
          DxTemplateModule, 
          DxButtonModule, 
          DxTreeMapModule,
          DxDataGridModule, 
          DxChartModule, 
          DxListModule, 
          DxTextBoxModule, 
          DxScrollViewModule} from 'devextreme-angular';


import { SecondDevComponent } from './second-dev/second-dev.component';
import { ThirdDevComponent } from './third-dev/third-dev.component';

@NgModule({
  declarations: [
    AppComponent,
    DevComponent,
    SecondDevComponent,
    ThirdDevComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxBulletModule,
    DxTemplateModule,
    DxButtonModule,
    DxTreeMapModule,
    DxDataGridModule,
    DxChartModule,
    DxListModule,
    DxTextBoxModule,
    DxScrollViewModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
