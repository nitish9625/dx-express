import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondDevComponent } from './second-dev.component';

describe('SecondDevComponent', () => {
  let component: SecondDevComponent;
  let fixture: ComponentFixture<SecondDevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondDevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
