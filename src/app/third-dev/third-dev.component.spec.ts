import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdDevComponent } from './third-dev.component';

describe('ThirdDevComponent', () => {
  let component: ThirdDevComponent;
  let fixture: ComponentFixture<ThirdDevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThirdDevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
